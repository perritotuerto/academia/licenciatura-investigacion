# *La fundación de Colima: lugar de encuentro y desencuentro de la historiografía regional*

Aquí se encuentra la ponencia presentada en el
«[VI Foro Colima y su región](http://culturacolima.gob.mx/v2/vi-foro-colima-y-su-region/)». 
El evento fue realizado por la Secretaría de Cultura del Estado de Colima
en 2011.
